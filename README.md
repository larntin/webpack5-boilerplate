# webpack5-boilerplate

极简 webpack5 脚手架，仅仅包含刚好够用的功能

所有配置中包含详细注释

# 工程介绍

基于 webpack5 的各类工程的样例

## webpack5-js

最简易、轻量的脚手架，包含以下：

1. webpack5
1. javascipt （Babel）
1. Less

## webpack5-react-lib

没有实现

## webpack5-ts-esbuild-antd5

通过 esbuild 提升性能，并包含了antd5

## webpack5-ts-react-router

【推荐】ts react router 最全面的框架

