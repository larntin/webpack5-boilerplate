import React from "react";
import { createHashRouter, Navigate } from "react-router-dom";
import SimpleLayout from "../layout/SimpleLayout";
import About from "../pages/About";
import Home from "../pages/Home";
import NotFoundPage from "../pages/NotFoundPage";

const routes = createHashRouter([
  {
    path: "/",
    element: <SimpleLayout />,
    errorElement: <NotFoundPage />,
    children: [
      {
        path: "home",
        element: <Home />,
      },
      {
        path: "about",
        element: <About />,
      },
    ],
  },
]);

export default routes;
