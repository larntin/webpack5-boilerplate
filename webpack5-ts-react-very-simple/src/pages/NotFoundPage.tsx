import React from "react";
import { useNavigate } from "react-router-dom";

const NotFoundPage: React.FC = () => {
  const navigate = useNavigate();

  return (
    <div>
      <div>访问的页面不存在</div>
      <button onClick={() => navigate("/home")}>Back Home</button>
    </div>
  );
};

export default NotFoundPage;
