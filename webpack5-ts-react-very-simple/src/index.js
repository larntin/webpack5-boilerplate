// import 'core-js/stable';
// import 'regenerator-runtime/runtime';

import React from "react";
import { createRoot } from 'react-dom/client';

import "normalize.css";
import "./main.less";

import App from './App';

const root = document.getElementById('root');
if (root) {
  createRoot(root).render(<App />);
}

// Note: Before npm run build the statement module.hot.accept(); could / should to be disabled / comment out !!!
// In Webpck HotModuleReplacementPlugin() is used to set hot to true.
// This way the browser dont need to reload the entire page when changing  file !
// Note: Needed here - in contrast to Vue.js  !!
if (module.hot) {
  module.hot.accept();
}
