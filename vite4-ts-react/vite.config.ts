import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react";

export default defineConfig((command, mode) => {
  const env = loadEnv(mode, process.cwd(), "");

  return {
    base: "./",

    plugins: [react()],

    // 设置最终构建的浏览器兼容目标。默认值是一个 Vite 特有的值：'modules',
    // 这是指 支持原生 ES 模块、原生 ESM 动态导入 和 import.meta 的浏览器。
    // Vite 将替换 modules 为 ['es2020', 'edge88', 'firefox78', 'chrome87', 'safari14']
    build: { target: ["ES2015", "chrome78"] },

    css: {
      preprocessorOptions: {
        // 为 Antd@^4.24.10 开启 less 的配置项
        less: {
          // 支持内联 JavaScript
          javascriptEnabled: true,
          // 重写 less 变量，定制样式
          modifyVars: {
            // 这里修改颜色的优先级最高
            // "@primary-color": "red",
          },
        },
      },

      // 配置 CSS modules 的行为。选项将被传递给 postcss-modules。
      // 源码库及配置：https://github.com/madyankin/postcss-modules
      modules: {
        // generateScopedName详细说明：https://github.com/webpack/loader-utils#interpolatename
        // 就是webpack 下的 loader-utils 工具的 interpolatename参数的文档
        generateScopedName: "[local]__[hash:base64:5]",
        // 加上 [name] 的前缀太长了
        // generateScopedName: "[name]--[local]__[hash:base64:5]",
        hashPrefix: "prefix",
      },
    },

    server: {
      port: env.PORT,
      proxy: {
        "/api": {
          target: "http://localost:9605/feng",
          pathRewrite: { "^/api": "" },
        },
      },
    },
  };
});
