import React from "react";
import ReactDOM from "react-dom/client";
// main 中引入了样式库，这里的先后顺序对 css 来说很重
import "./main.less";
import App from "./App.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
