import React from "react";
import { Outlet } from "react-router-dom";
import styles from "./BlankLayout.module.less";

const BlankLayout: React.FC = () => {
  return (
    <div className={styles.blankLayout}>
      <Outlet />
    </div>
  );
};

export default BlankLayout;
