import { useState } from "react";
import { RouterProvider } from "react-router-dom";
import router from "./router/router";
import "./App.less";

function App() {
  return (
    // 简写的意义：vite-react-app 
    <div className="v-r-app">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
