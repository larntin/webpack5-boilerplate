import React from "react";
// 引用 src/assets 下的svg文件
import reactLogo from "../../assets/react.svg";
// 引入 public 下的 svg 文件
// https://cn.vitejs.dev/guide/assets.html#public 文档中说：
// 1. 开发时能直接通过 / 根路径访问到，并且打包时会被完整复制到目标目录的根目录下
// 2. public 中的资源不应该被 JavaScript 文件引用
import viteLogo from "/vite.svg";
import styles from "./Home.module.less"
import { Button, Input } from "antd";

export default function Home() {
  return (
    <div className={styles.home}>
      <a href="https://vitejs.dev" target="_blank">
        <img src={viteLogo} alt="Vite logo" />
      </a>
      <a href="https://react.dev" target="_blank">
        <img src={reactLogo} alt="React logo" />
      </a>

      <Input></Input>

      <Button>测试</Button>
    </div>
  );
}
