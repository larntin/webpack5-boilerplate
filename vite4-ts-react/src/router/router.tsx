import React from "react";
import { createHashRouter, Navigate } from "react-router-dom";
import BlankLayout from "../layout/BlankLayout";
import Home from "../pages/home/Home";

const router = createHashRouter([
  {
    path: "/",
    element: <BlankLayout />,
    children: [
      {
        index: true,
        path: "home",
        element: <Home />,
      },
      {
        path: "",
        element: <Navigate to="home" replace />,
      },
    ],
  },
  {
    path: "*",
    element: <Navigate to="/" replace />,
  },
]);

export default router;
