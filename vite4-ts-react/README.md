# Vite4 + TS + React

1. vite@^4.4.5
2. Typescript@^5.0.2
3. React@^18.0.2
4. 前端开发组件如下
   1. react-router-dom@^6.17.0

## 扩展 ESLint

[扩展 ESLint 的文档](https://zh-hans.eslint.org/docs/latest/extend/ways-to-extend)

# 配置简介

## Antd + less

1. `vite@4` 的项目自动识别 less 和 sass，只需要项目中引入 less 或者 sass 即可，我们引入了 less （因为使用的市 Antd@4.x）
   1. 也可以修改成 Antd@^5
2. css module 的基本使用方式
   1. css 文件名带 `*.module.less` 会自动编译成 vite.config.ts 配置文件中 css module 设置的样子

## 静态资源

[静态资源文档](https://cn.vitejs.dev/guide/assets.html#public) 中说：

1. 开发时能直接通过 / 根路径访问到，并且打包时会被完整复制到目标目录的根目录下
2. 必须保持原有文件名（没有经过 hash）
3. public 中的资源不会被源码引用，也`不应该被 JavaScript 文件引用`

注意：非要引入的话，引入 public 中的资源永远应该使用根绝对路径 —— 举个例子，public/icon.png 应该在源码中被引用为 /icon.png。

## 公共基础路径

1. 需要在嵌套的公共路径下部署项目，只需指定 base 配置项, `base 默认值：/`，
   1. 通过命令行修改 `vite build --base=/my/public/path/`
   2. 通过 vite.config.ts 修改 `base: "./"`

## vite 环境变量

环境变量必须以 `VITE_` 开头。

1. Vite 在一个特殊的 import.meta.env 对象上暴露环境变量:
   - import.meta.env.MODE: {string} 应用运行的模式。
   - import.meta.env.BASE_URL: {string} 部署应用时的基本 URL。他由 base 配置项决定。
   - import.meta.env.PROD: {boolean} 应用是否运行在生产环境。
   - import.meta.env.DEV: {boolean} 应用是否运行在开发环境 (永远与 import.meta.env.PROD 相反)。
   - import.meta.env.SSR: {boolean} 应用是否运行在 server 上。
2. 在生产环境中，这些环境变量会在构建时被静态替换，因此，在引用它们时请使用完全静态的字符串。动态的 key 将无法生效。
   1. 例如，动态 key 取值 import.meta.env[key] 是无效的。
   2. 如当 "process.env.NODE_ENV" 被替换为 ""development": "，解决问题：
      1. 对于 JavaScript 字符串，你可以使用 unicode 零宽度空格来分割这个字符串，例如： 'import.meta\u200b.env.MODE'
      2. 对于 Vue 模板或其他编译到 JavaScript 字符串的 HTML，你可以使用 <wbr> 标签，例如：import.meta.<wbr>env.MODE
3. .evn 文件，Vite 使用 dotenv 从你的 环境目录 中的下列文件加载额外的环境变量：
```
.env                # 所有情况下都会加载
.env.local          # 所有情况下都会加载，但会被 git 忽略
.env.[mode]         # 只在指定模式下加载
.env.[mode].local   # 只在指定模式下加载，但会被 git 忽略
```
4. import.meta.env 和 loadEnv 的使用场景不同
   1. import.meta.env 是在运行时获取环境变量的值，适用于应用程序代码中需要动态获取环境变量的场合
   2. loadEnv 则是在构建时加载环境变量，适用于打包时（构建时）需要引用环境变量的场合