module.exports = [
  {
    url: '/api/sso/login/checkStatus',
    type: 'get',
    response: {
      code: 200,
      message: 'success',
      data: {
        login: true,
        binding: true,
        tokenId: 'tokenId380530530'
      }
    }
  },
  {
    url: '/api/user/info',
    type: 'get',
    response: {
      code: 200,
      message: 'success',
      data: {
        name: 'shawnxiao',
        avatar: '',
        account: 'account',
        mobile: 'mobile',
        role: 'role',
        id: 'id'
      }
    }
  }
];
