/* eslint-disable @typescript-eslint/no-var-requires */
const { handleMockArray } = require('./utils');
const mockData = [];
const mockArray = handleMockArray();
mockArray.forEach(item => {
  const obj = require(item);
  mockData.push(...obj);
});
function Mock(app) {
  mockData.forEach(item => {
    app[item['type']](item['url'], (req, res) => {
      res.json(item['response']);
    });
  });
  // app.get('/xxxx/yyy', (req, res) => {
  //   console.log('getPower111')
  //   res.json('getPower')
  // })
  // app.post('/reconfig', (req, res) => {
  //   console.log('reConfig111')
  //   res.json('reConfig')
  // })
}

module.exports = Mock;
