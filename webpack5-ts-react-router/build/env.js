// 环境变量值配置
module.exports = {
  'dev': {
    'BASEURL': 'https://baidu.dev.com/api',
    'APP_LOGIN_URL': '//app.xxx.com',
    'APP_BASE_API': '//app.xxx.com',
  },
  'test': {
    'BASEURL': 'https://baidu.test.com/api',
    'APP_LOGIN_URL': '//app.xxx.com',
    'APP_BASE_API': '//app.xxx.com',
  },
  'pre': {
    'BASEURL': 'https://baidu.test.com/api',
    'APP_LOGIN_URL': '//app.xxx.com',
    'APP_BASE_API': '//app.xxx.com',
  },
  'prod': {
    'BASEURL': 'https://baidu.prod.com/api',
    'APP_LOGIN_URL': '//app.xxx.com',
    'APP_BASE_API': '////app.xxx.com',
  }
}
