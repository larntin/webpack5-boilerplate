import React from 'react';
import { Outlet } from 'react-router-dom';
import styles from './SimpleLayout.less';

const SimpleLayout: React.FC = () => {
  return (
    <div className={styles.simpleLayout}>
      <Outlet />
    </div>
  );
};

export default SimpleLayout;
