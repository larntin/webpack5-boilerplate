import React from 'react';
import { RouterProvider } from 'react-router-dom';
import routers from './routes/routes';

// 全局样式，使用了 :global 关键字
import './app.less';

function App() {
  console.log('NODE_ENV', process.env.NODE_ENV, process.env.BASEURL);
  console.log('BASE_ENV', process.env.BASE_ENV);

  return <RouterProvider router={routers} />;
}

export default App;
